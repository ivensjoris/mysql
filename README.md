# Ola humanos, sejam bem vindos

> Para saber mais sobre banco de dados e MySql clique no link abaixo  
[Banco de Dados e MySql](./MYSQL.md)

> Para saber sobre a conexão e arquitetura basee entre **PHP** e MySql clique abaixo  
[Conectando Php com MySql](./PHP.md)

Dicas para criação de ambiente
 - Recomendado ter PHP com versão maior ou igual a 7.2
 - Recomendado ter mariaDb ou MySql versão mais recente disponivel no package manager de seu sistema
 - Apesar de ainda não ser necessario, recomendo a instalação do composer( instale na pasta bin e com o nome de composer )
 - Se alguem tiver dificuldade escrevo o passo a passo de instalação do ambiente tanto em linux quando em windonws
 - Mesmo tendo visto a capacitação recomendo lê os arquivos para pegar nuances não comentados e recapitular tudo

> - O arquivo codigo1.php tem uma implementação basica da conexão do php com mysql, olhe que as variaveis de conexão estão no arquivo ./admin/dbconfig.php  

> - O arquivo index.php utiliza os arquivos da pasta models, que é a explicação feita no markdown de [PHP](./PHP.md)

> - Tambem existe um script em python para vê que a interface basica de conexão com bancos de dados é parecida com varias linguagens(eu queria colocar em node, mas o python já vem instalado na maioria dos sistemas e não precisa de conhecimento previo para sua utilização)


Para ter um banco de dados de exemplo, instale o script .sql deste repositorio com
```shell
$ mysql -u seu_user -p < ./mysqlsampledatabase.sql
```

## Qualquer duvida, dificuldade, ou sugestão, abra uma issue ou peça ajuda a alguem :)
