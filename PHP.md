# PHP

> Vamos codar hehe  

## PDO (PHP Data Objects)  
Usaremos o PDO para conectar ao MySql pelo PHP e fazer as manipulações.  
PDO é uma classe desenvolvida especificamente para trabalhar com procedimentos relacionados a Banco de Dados. O interessante em utilizar este tipo de classe é a abstração de qual banco utilizamos e a segurança extra que esta classe nos oferece.  
[Usando PDO](https://www.devmedia.com.br/usando-pdo-php-data-objects-para-aumentar-a-produtividade/28446)

- **Flexibilidade** – Como o PDO carrega o driver específico do banco de dados em tempo de execução, não é preciso reconfigurar o PHP sempre que um banco de dados diferente for usado.

- **Desempenho** – O PDO está escrito em C e compilado no PHP, o que lhe garante um aumento considerável no desempenho em relação a soluções escritas em PHP.

- **Consistência de código** – No PDO não existe a inconsistência de código, pois é oferecida apenas uma interface unificada que é está disponível para qualquer banco de dados.

- **Características de orientação de objetos** – Possui recursos de orientação de objetos, o que resulta em uma comunicação mais poderosa e eficiente com banco de dados.

[Sobre PDO](https://www.devmedia.com.br/introducao-ao-php-data-objects-pdo/25318)

> É óbvio que não basta só aplicar o PDO e tudo estará seguro e perfeito. Você precisa também implementar algum padrão de projeto que implemente o conceito de multicamadas, onde o principal objetivo é separar a camada de dados (onde ficam as instruções SQL) do restante da aplicação. E qual a finalidade disso juntamente com o PDO? Separando a sua aplicação em camadas, e utilizando o PDO adequadamente, você terá a receita perfeita de uma aplicação bem implementada e estruturada.

## Como utilizar o PDO?

1. Tenha o PHP
2. Talvez você tenha de configurar o PHP.ini para ativar (eu não precisei  :simple_smile: )
3. Codar
4. Usar uma camada de abstração para ajudar a aplicação a crescer, ficar bonita e profissional
> Olha que senior isso

Para conectar ao DataBase com o PDO é algo bem izy:  

```php
<?php
  $conn = new PDO('mysql:host=localhost;dbname=meuBancoDeDados', $username, $password);
?>
```  
Podemos fazer melhor, utilizar tratamento de erros:  

```php
<?php
try {
    $conn = new PDO('mysql:host=localhost;dbname=meuBancoDeDados', $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}
?>
```  
Para consultar agora, poderiamos simplesmente fazer isso:  

```php
$name = 'Alice'
$data = $conn->query('SELECT * FROM minhaTabela WHERE nome = ' . $conn->quote($name));
  
foreach($data as $row) {
    print_r($row); 
}
```  
Porem isso é algo chato, além que teriamos de fazer isso para varios valores, logo tem os `PREPARED STATEMENTS`, imagine buscar dados de um fomulario colocando os dados com a função `$conn->quote($var)`:  

```php
$conn = new PDO('mysql:host=localhost;dbname=meuBancoDeDados', $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);    
	
$stmt = $conn->prepare('SELECT * FROM minhaTabela WHERE id = :id');
$stmt->execute(array('id' => $id));

while($row = $stmt->fetch()) {
	print_r($row);
}
```  
Isso também protege um pouco de SQL injection, uma forma alternativa de fazer a mesma coisa é:  

```php
$stmt->bindParam(':id', $id, PDO::PARAM_INT);
$stmt->execute();
```  
Normalmente ao usar o fetch, o padrao é o retorno `PDO::FETCH_ASSOC`, o qual retorna uma matriz, mas podemos substituir o que poderiamos receber de retorno:
- PDO::FETCH_ASSOC: Retorna uma matriz.
- PDO::FETCH_BOTH: Retorna uma matriz, indexada pelo nome da coluna e 0-indexados.
- PDO::FETCH_BOUND: Retorna TRUE e atribui os valores das colunas no seu conjunto de resultados para as variáveis ​​PHP que estavam amarradas.
- PDO::FETCH_CLASS: Retorna uma nova instância da classe especificada.
- PDO::FETCH_OBJ: Retorna um objeto anônimo, com nomes de propriedades que correspondem às colunas.  

```php
$stmt->fecth(PDO::FETCH_OBJ)
```  
Também podemos buscar todos os dados, e assim retornar os resultados para o usuario:  

```php
$result = $stmt->fetchAll();
  
if ( count($result) ) { 
foreach($result as $row) {
    print_r($row);
}   
} else {
    echo "resultado não retornado.";
}
```  

Tudo feitoo!!  

## Models
> Para que models ?
Models é uma camada de abstração do modelo MVC, Model View Controller.  
![MVC express](./assets/images/MVC.png)  
Apesar de essa imagem ser sobre implementação em outra tecnologia( é a minha tecnologia favorita :3)
o padrão de projeto tem o mesmo sentido e implementação quase igual quando o backend envia a viewer.
Isso ajudará a aplicação a escalar, já que terá um entendimento mais facil por pessoas que conhecem o mesmo padrão, e ser mais facil de debugar, melhor usar algo pensado e estudado por alguem que se esforçou nisso do que inventar algo só para funcionar.  
Note que o MVC foca em separar a aplicação em partes, dando responsabilidade para cada um modulo, assim fica mais facil de se entender o que uma parte separada faz, por agora faremos o model, a função do model é ditar como a aplicação deve se conectar/utilizar o banco de dados.  
Os outras partes importantes é o controller que faz comanda a regra de negocio da aplicação e a viewer que tem responsabilidade na parte de apresentação da aplicação.

### Teremos uma classe de conexão:
```php
class Conexao
{

    public static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new PDO("mysql:host=localhost;dbname=learn_php", "newuser", "user_password",
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$instance->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
        }

        return self::$instance;
    }

}
```

### Um POJO
Ou Melhor POPO:  
A POPO serve para guardar dados de um usuario que tem no banco, note como é facil apenas fazer `Mario.getEmail()`, assim você pode ter um objeto para cada usuario.  
Note também que é uma POPO é uma classe simples, apenas tem campos e ses getters e setters  
```php
<?php
class User {

    private $id;
    private $email;
    private $password;
    private $nickname;

    public function __construct(array $array = []) {
        $this->id = isset($array['id']) ? $array['id'] : null;
        $this->email = isset($array['email']) ? $array['email'] : null;
        $this->password = isset($array['password']) ? $array['password'] : null;
        $this->nickname = isset($array['nickname']) ? $array['nickname'] : null;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    public function getNickname() {
        return $this->nickname;
    }

    public function setNickname($nickname) {
        $this->nickname = $nickname;
        return $this;
    }

    public function toArray() {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'password' => $this->password,
            'nickname' => $this->nickname,
        ];
    }

    public function toJson() {
        return json_encode($this->toArray());
    }

}
```

Uma utilização simples da nossa classe  
```php
$user = new User();
// or
$user = new User([
    'id' => 2,
    'email' => 'toto@gmail.com'
]);
```

### E por ultimo o DAO
DAO significa Data Acess Object, ele é a peça que orquestra nossos dados com nossa aplicação, ela se conecta com o banco e faz as query que precisamos injetando os dados na nossa POPO.  
No fim é só importar a DAO em nosso Controller(ou no index.php kkk) e utilizar.  

```php
class DaoUsuario
{

    public static $instance;

    private function __construct()
    {
        //
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new DaoUsuario();
        }

        return self::$instance;
    }

    public function Inserir(PojoUsuario $usuario)
    {
        try {
            $sql = "INSERT INTO usuario (
                nome,
                email,
                senha,
                ativo)
                VALUES (
                :nome,
                :email,
                :senha,
                :ativo)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(":nome", $usuario->getNome());
            $p_sql->bindValue(":email", $usuario->getEmail());
            $p_sql->bindValue(":senha", $usuario->getSenha());
            $p_sql->bindValue(":ativo", $usuario->getAtivo());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(PojoUsuario $usuario)
    {
        try {
            $sql = "UPDATE usuario set
        nome = :nome,
                email = :email,
                senha = :senha,
                ativo = :ativo,
                WHERE cod_usuario = :cod_usuario";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(":nome", $usuario->getNome());
            $p_sql->bindValue(":email", $usuario->getEmail());
            $p_sql->bindValue(":senha", $usuario->getSenha());
            $p_sql->bindValue(":ativo", $usuario->getAtivo());
            $p_sql->bindValue(":cod_usuario", $usuario->getCod_usuario());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Deletar($cod)
    {
        try {
            $sql = "DELETE FROM usuario WHERE cod_usuario = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod)
    {
        try {
            $sql = "SELECT * FROM usuario WHERE cod_usuario = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaUsuario($p_sql->fetch(PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    private function populaUsuario($row)
    {
        $pojo = new PojoUsuario;
        $pojo->setCod_usuario($row['cod_usuario']);
        $pojo->setNome($row['nome']);
        $pojo->setEmail($row['email']);
        $pojo->setSenha($row['senha']);
        $pojo->setAtivo($row['ativo']);
        return $pojo;
    }

}
```
