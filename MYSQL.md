# Banco de Dados

## O que é banco de dados? Para que serve? É de comer?

![Banco de Dados](./assets/images/banco-de-dados.jpeg)

> “Bancos de dados, ou bases de dados (em Portugal),são coleções de informações
> que se relacionam de forma que crie um sentido. São de vital importância para empresas,
> e há duas décadas se tornaram a principal peça dos sistemas de informação.“  
> Fonte: Wikipedia

Os Bancos de Dados estão presentes em nossa vida a muito tempo, um exemplo disso é a lista telefonica.  
Algumas empresas tem milhares de documentos em papel para guardar seus dados.  
Hoje em dia podemos guardar isso em computadores, e ter beneficios em sua manipulação e armazenação.
Podemos simplificar essa definição para: "Bancos de dados são coleções de dados interligados entre si e organizados para fornecer informações."  
Existem Dados, Informações e MetaDados.  
Dados é um fato bruto  
Informação é um conjunto de dado corelacionado que tem algum sentido  
Metadado é o dado relativo a outro dado.  

> Dado  
> 2001  

> Informação  
> Ano do atentado terrorista às torres gêmeas: 2001  

> Metadado  
> Ano do atentado terrorista às torres gêmeas  


**(link 2)**

- ### **DBMS ou SGDB**
	DBMS é Database Management Systems ou Sistema Gerenciador de Banco de Dados(SGDB).  
	São softwares que lhe fornecem maneiras de armazena e manipular *dados*, atualmente no mercado existem diversos sistemas, os mais usados são definidos como sistemas SQL, e na ultima decada apareceu os NOSQL, cada um tem algum proposito e objetivo.
- ### SQL

	> Structured Query Language, ou Linguagem de Consulta Estruturada ou SQL, é a linguagem de pesquisa declarativa padrão para banco de dados relacional (base de dados relacional).  
    > Muitas das características originais do SQL foram inspiradas na álgebra relacional.

	> Fonte: Wikipedia

	![Survey StackOverflow Languages](./assets/images/SO-survey-languages.png)

	Basicamente é uma linguagem que serve para manipular os bancos de dados relacionais, e os bancos
	tidos como SQL são os que seguem esse padrão.
	Bancos nesse padrão conhecidos são o MySql, Postgre Sql, MSsql, ORACLE DB.
	![Survey StarckOverflow Databases](./assets/images/SO-survey-databases.png)

- ### NOSQL
	NOSQL significa NOT ONLY SQL, é um guarda chuva que abranje uma diversidades de banco de dados criados para propositos diferentes dos bancos já existentes.
	Eles deveriam ser conhecidos como NOREL, por a maioria não trabalhar bem com dados relacionais.  
	Eles podem ser utilizados para cacheamente (Redis), para armazenar dados com muitoo relacionamento(Neo4j), armazenar dados em forma de documento(MongoDb).  
	Eles servem para qualquer aplicação então?  
	**Não**  
	A maioria das aplicações tem dados relacionados, e os bancos sql supre a necessidade, usar algo diferente talves tenha de ter como um pequeno obstaculo isso, normalmente quando as empresas grandes usam, elas sabem que não terá concistencia em seus dados, já abriu o facebook e viu a foto de uma pessoa trocada por um momento? são tradeoffs a serem levados em conta.  

---

## Agenda  

- [x] Conceitos gerais de banco de dados: o que é, SGBDs, etc.
- [ ] Regras (pk, fk, un, uq, nn, etc.)
- [ ] SQL: Demonstração e prática do CRUD (com recursos e regras de cada uma), além de funções adicionais acerca dele.
- [ ] Conceitos avançados: chaves primárias e estrangeiras,
- [ ] PDO: o que é e como funciona
- [ ] PDO: Conexão do PHP com o banco de dados
- [ ] Conceito sobre Models
- [ ] Execução de queries a partir do PHP
- [ ] Atividade: Criar uma conexão do banco de dados com as páginas desenvolvidas e um CRUD com as postagens

---

## Como funciona um banco como o MySql

Trabalharemos com tabelas que armazenam os dados, e varias tabelas juntas podem conter relacionamentos

> Hero table

 | id  | nome          | poder                             | live-in |
 | --- | ------------- | --------------------------------- | ------- |
 | 1   | batman        | ele é top                         | 3       |
 | 2   | superman      | naceu com poderes, e rico         | 1       |
 | 3   | homem aranha  | solta teia, gruda, sentido aranha | 2       |
 | 4   | homem formiga | ficar pequeninim ou gandao        | 5       |

> City table

 | id  | nome        |
 | --- | ----------- |
 | 1   | Metrópolis  |
 | 2   | Nova Iorque |
 | 3   | Gotham City |
 | 4   | Townsville  |

### Table CRUD (Create, Read, Update, Delete)

> Para vê os bancos que existem use `SHOW DATABASES`,  
> Para vê as tabelas de um banco use `SHOW TABLES`,  
> Para vê melhor uma tabela use `DESCRIBE table_name`.  

Antes de Tudo claro, precisamos criar um banco de dados:  
```sql
CREATE DATABASE [IF NOT EXISTS] database_name;
```  
```sql
USE database_name;
```  
```sql
DROP DATABASE [IF EXISTS] database_name;
```  

---

1. **Tabela**  
Agora com nosso banco criado e aberto, podemos criar as tabelas que terão nossos dados né?  
Vamos:  
```sql
CREATE [TEMPORARY] TABLE [IF NOT EXISTS] table(
  key type(size) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  c1 type(size) NOT NULL,
  c2 type(size) NULL,
...
);
```  
> [Saiba mais sobre temporary table](http://www.mysqltutorial.org/mysql-temporary-table/)  

Meio sem sentido? veja funcionando:  
```sql
CREATE TABLE IF NOT EXISTS trainees(
  trainee_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nome VARCHAR(30) NOT NULL,
  diretoria VARCHAR(15) NULL
);
```  
<br/>

Você também pode alterar a tabela depois de criada, e adicionar ou remover colunas,  
ou até deletar a tabela em si.  
Para alterar o comando é `ALTER TABLE` seguido do nome da tabela, seguido de `ADD` ou `DROP`:  
```sql
ALTER TABLE table_name ADD [PRIMARY KEY] column_name;
ALTER TABLE table_name DROP column_name;
ALTER TABLE table_name DROP PRIMARY KEY;
```
```sql
DROP TABLE [IF EXISTS] table [, name2, ...]
```
```sql
DESCRIBE table [column];
```

Prontoo, aprendemos a fazer as tabelas, e agora? como que colocamos nossos dados?

---

2. **Dados**  
> Para inserir dados é o comando `INSERT INTO` :  
```sql
INSERT INTO table1 (field1, field2, ...) 
VALUES (value1, value2, ...),
(value1, value2, ...)
```
Se tiver datas, coloque como `YYYY-MM-DD`
```sql
INSERT INTO tasks(title, start_date, due_date)
VALUES('Insert date into table','2018-01-09','2018-09-15');
```
ou use a função `CURRENT_DATE()`  
```sql
INSERT INTO tasks(start_date)
VALUES(CURRENT_DATE())
```
> Para Atualizar os dados é o comando `UPDATE`  
```sql
UPDATE [LOW_PRIORITY] [IGNORE] table_name 
SET 
    column_name1 = expr1,
    column_name2 = expr2,
    ...
[WHERE
    condition];
```
Por exemplo:
```sql
UPDATE employees 
SET 
    email = 'mary.patterson@classicmodelcars.com'
WHERE
    employeeNumber = 1056;
```
A clausura `WHERE` será explicada melhor depois, é parecido com um `if`, se omitir ela, todas as linhas seráo alteradas
UPDATE customers  

> Agora que aprendemos a Criar dados, e a Atualizar, como que deletamos eles ?  

Com a expressão `DELETE`
```sql
DELETE FROM table_name
[WHERE condition];
```
Creio que seja bem sujestivo o funcionamento  
Exemplo mais "complexo" :  
```sql
DELETE FROM customers
WHERE country = 'France'
ORDER BY creditLimit
LIMIT 5;
```

---

E agora, a parte mais importante, Seleção de Dados, Query Selector, a linguagem de seleção, o que o SQL significaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa  
Go go go?  

> A unica operação que falta do CRUD para nossos exemplos é o `SELECT` (que seria o Read do CRUD), para pegar um dado do banco de dados SQL utilizamos a expressão `SELECT` :  

Para vermos tudo que uma tabela temos usamos :  
```sql
SELECT * FROM table_name;
```
A sintaxe do `SELECT` é :  
```sql
SELECT [DISTINCT]
    column_1 AS coluna,
    column_2,
    ...
FROM
    table_1
[INNER | LEFT |RIGHT] JOIN table_2 ON conditions
WHERE
    conditions
GROUP BY column_1
HAVING group_conditions
ORDER BY column_1
LIMIT offset, length;
```
assim podemos vê que tem bastante coisa, mas vamos mostrar devagar o que cada palavrinha significa, vamos lá.  
1. `SELECT` recebe como argumentos uma lista de colunas separadas por virgula, ou um asterisco que significa todas as colunas
2. `FROM` especifica a tabela que pesquisamos os dados
3. `JOIN` obtem dados de outras tabelas especificas os argumentos colocados
4. `WHERE` está clausura filtra os dados retornados
5. `GROUP BY` agrupa os dados retornados de acordo ` aggregate functions`
6. `HAVING` filtra grupos de informações definidos por `GROUP BY`
7. `ORDER BY` especifica como ordenar o resutado
8. `LIMIT` limita o número de linhas retornadas

O `DISTINCT` depois do `SELECT` faz com que não venha resultado duplicado  
O `AS` depois do nome da coluna, cria uma aliase na demonstração da query

> Ordenando a query: 
```sql
SELECT column1, column2,...
FROM tbl
ORDER BY column1 [ASC|DESC], column2 [ASC|DESC],...
```
O `ORDER BY` irá ordenar os dados, o ASC significa do menor para o maior, e DESC é o contrario de ASC.
```sql
SELECT country FROM customers ORDER BY country ASC;
```
Se declarar mais de duas `ORDER BY` a proxima ordenção ocorrera quando tiver repetição.  
Também é possivel ordenar de acordo algum set de informações definidas.
```sql
SELECT 
    orderNumber, status
FROM
    orders
ORDER BY FIELD(status,
        'In Process',
        'On Hold',
        'Cancelled',
        'Resolved',
        'Disputed',
        'Shipped');
```

> Filtrando a query:  

Para filtrar uma consulta de dados, utilizamos o `WHERE`,
```sql
SELECT 
    select_list
FROM
    table_name
WHERE
    search_condition;
```
E a magica ocorre na confição do `WHERE`, não se preocupe, é parecido com condinções booleanas.  
Procurando dados onde um dado é igual a uma condição: 
```sql
SELECT
    lastname,
    firstname,
    jobtitle
FROM
    employees
WHERE
    jobtitle = 'Sales Rep';
```
para combinar multiplas expressões booleanas, é só utilizar o `AND` ou o `OR`
```sql
SELECT * FROM employees
WHERE
    jobtitle = 'Sales Rep' AND 
    officeCode = 1 OR
	lastname = 'Murphy';
```
os simbolos utilizados para filtrar são:
| simbolo  | significado                                                                    |
| -------- | ------------------------------------------------------------------------------ |
| =        | Igual a. você pode usar isso com qualquer tipo de dado                         |
| <> ou != | Não igual a (Diferente)                                                        |
| <        | Menor que. você normalmente utilizará isso com tipos númericos e de data/tempo |
| >        | Maior que                                                                      |
| <=       | Menor que ou igual a                                                           |
| =        | Maior que ou igual a                                                           |

Para utilizar a expressão `WHERE` de forma complexa, poderemos utilizar as termos: 
- BETWEEN seleciona os valores entre um range de valores .
- LIKE seleciona os valores de acordo um padrão ou regex.
- IN seleciona um valor de acordo um set.
- IS NULL checa se o valor é NULL.

```sql
SELECT 
    productCode, 
    productName, 
    buyPrice
FROM
    products
WHERE
    buyPrice BETWEEN 90 AND 100;
```
```sql
SELECT 
    employeeNumber, 
    lastName, 
    firstName
FROM
    employees
WHERE
    firstName LIKE 'a%';
	# lastname LIKE '%on%';
	# firstname LIKE 'T_m';
	#lastName NOT LIKE 'B%';
```

```sql
SELECT 
    officeCode, 
    city, 
    phone, 
    country
FROM
    offices
WHERE
    country IN ('USA' , 'France');
    # country NOT IN ('USA' , 'France');
    # country = 'USA' OR country = 'France';
```

```sql
SELECT customerName, country, salesrepemployeenumber
FROM customers
WHERE
    salesrepemployeenumber IS [NOT] NULL
ORDER BY customerName; 
```

---

> E agora um conteúdo que parece um pouco complicado, mas é muito simples e ajuda muito que é o JOIN, porém antes vamos explicar sobre Chaves

### Chaves(Key)

1.  ### primary key
	Chave primaria é uma coluna ou um conjunto de colunas que unicamente
	identifica cada linha em uma tabela.
	A chave primaria deve conter valores unicos, se a tabela contém multiplas
	colunas de chave primaria, a combinação desses valores deve ser unico
	Ela deve ser não nula, é bom sempre declarar `NOT NULL` na criação dela(caso não declarado o mysql declara isso implicetamente)

	Como o MySql trabalha rapido com números, a chave primaria deve ser numeros, `INT`, `BIGINT`.
	Tenha certeza que o range definido suporte todos os dados

	#### Definindo chave primaria no MySql
	Normalmente é definido na criaçãoda tabela
	```sql
	CREATE TABLE users(
	  user_id INT AUTO_INCREMENT PRIMARY KEY,
	  username VARCHAR(40),
	  password VARCHAR(255),
	  email VARCHAR(255)
	);
	```
	ou
	```sql
	CREATE TABLE roles(
	  role_id INT AUTO_INCREMENT,
	  role_name VARCHAR(50),
	  PRIMARY KEY(role_id)
	);
	```
	Se o banco tiver multiplas colunas para chave primaria, elas devem ser definidas no fim
	do `CREATE TABLE` com `PRIMARY KEY(column name 1, column name 2)`  
	Se você deseja que a tabela tenha uma coluna que tenha valores unicos, e que eles não seja uma chave primaria( a qual contém algumas regras a cumprir), pode utilizar o termo `UNIQUE` igual utilizaria o termo `PRIMARY KEY`, ele faz com que os valores sejam unico porem sem as restrições de uma PK.
2. ### Foreign Key
	Chaves estrangeiras é um campo na tabela que se conecta com outras tabelas, lembra do exemplo de relações mostrado lá em cima ?
	```sql
	CREATE DATABASE IF NOT EXISTS dbdemo;
	
	USE dbdemo;
	
	CREATE TABLE categories(
	   cat_id int not null auto_increment primary key,
	   cat_name varchar(255) not null,
	   cat_description text
	) ENGINE=InnoDB;
	
	CREATE TABLE products(
	   prd_id int not null auto_increment primary key,
	   prd_name varchar(355) not null,
	   prd_price decimal,
	   cat_id int not null,
	   FOREIGN KEY fk_cat(cat_id)
	   REFERENCES categories(cat_id)
	   ON UPDATE CASCADE
	   ON DELETE RESTRICT
	)ENGINE=InnoDB;
	```
	![customers orders table](./assets/images/customers_orders_tables.png)  
	A fk_cat no tabela products é uma chave estrangeira, ela referencia para o id de uma categoria(eu achei q era gatos), assim podemos saber qual a categoria de um produto, note que uma categoria tem varios produtos, mas um produto tem apenas uma categoria.  
	Os statements `ON UPDATE` e `ON DELETE` configuram a ação que ocorre quando atualizamos ou deletamos um produto da tabela, se estas clausulas forem omitidas o mysql negara o pedido.  
	Se o valor for `CASCADE` a ação ocorre em cascata, por exemplo quando deleto um produto e o `ON DELETE` estive com `CASCADE` a categoria será deletada junto em cascata(imagina um domino derrubando o outro). O que salva tempo em ter de utilizar multiplas explessões de `DELETE` ou utilizar o `DELETE JOIN`.  
	Tambem existe o `SET NULL` que seta null na tabela filha quando uma tabela parente é deletada ou atualizada.  
	Existem também os valores `NO ACTION` e `RESTRICT`, se colocar eles o MySql irá rejeitar qualquer pedido de deletar ou atualizar as linhas.  

Sabendo os conceitos de `PRIMARY KEY` e `FOREIGN KEY` podemos vê sobre `JOIN`, que basicamente é a forma de dar `SELECT` entre tabelas relacionadas e obter bonitinho os resultados da consulta.

O MySql suporta os seguintes tipos de joins:
1. Cross join
2. Inner join
3. Left join
4. Right join

- ## Cross join
    Em um colégio, dez alunos candidataram-se para ocupar os cargos de presidente e vice-presidente do grêmio estudantil. De quantas maneiras distintas a escolha poderá ser feita?  
	```sql
	SELECT t1.id, t2.id
    FROM 
	  t1
    CROSS JOIN 
	  t2;
	```
    O `CROSS JOIN` faz a combinação entre os elementos da tabela, imagine que temos uma tabela com os alunos que se condidataram e uma tabela com os cargos, o CROSS JOIN fará essa combinação  
    ![Cross Join](./assets/images/MySQL-Join-cross-join-illustration.png)
- ## Inner join
    ```sql
    SELECT t1.id, t2.id
    FROM
        t1
            INNER JOIN
        t2 
            ON t1.pattern = t2.pattern;
    ```
    O `INNER JOIN` recebe um predicado e combina duas tabelas de acordo esse predicado,
    imagine o exemplo dos herois e da cidade, o inner join juntara tudo, porém se em alguma tabela o predicado falhar ele ficara de fora da consulta(por exemplo existir cidades a mais que herois, não aparecera nenhuma linha a mais mostrando as cidades)
    ![Inner Join](./assets/images/MySQL-Join-inner-join-illustration.png)

- ## Left join
    ```sql
    SELECT 
        t1.id, t2.id
    FROM
        t1
            LEFT JOIN
        t2 
            ON t1.pattern = t2.pattern
    ORDER BY t1.id;
    ```
    O `LEFT JOIN` é exatamente parecido com o `INNER JOIN`, a não ser que ele mostra todas as linhas, e quando algum valor na tabela da esquerda não corresponde a algo na tabela da direita, aparece `NULL` na consulta
    ![Left Join](./assets/images/MySQL-Join-left-join-illustration.png)

- ## Right join
    ```sql
    SELECT 
        t1.id, t2.id
    FROM
        t1
            RIGHT JOIN
        t2 
            on t1.pattern = t2.pattern
    ORDER BY t2.id;
    ```
    Já o `RIGHT JOIN` é parecido com o `LEFT JOIN`, porém dessa vez aparece `NULL`, na tabela da direita se a algum valor da tabela da direita não corresponder a algo na tabela da esquerda
    ![Right Join](./assets/images/MySQL-Join-right-join-illustration.png)

- ## Com chaves caso tenha curiosidade
    ```sql
    SELECT 
        orderNumber, 
        productName, 
        msrp, 
        priceEach
    FROM
        products p
            INNER JOIN
        orderdetails o ON p.productcode = o.productcode
            AND p.msrp > o.priceEach
    WHERE
        p.productcode = 'S10_1678';
    ```  
    [Qual a diferença entre relacionar 2 tabelas utilizando e não utilizando uma foreign key?](https://pt.stackoverflow.com/questions/71619/mysql-join-com-ou-sem-foreign-key)

---

- ## tambem existe algo parecido, o `UNION`, que é o qual junta uma tabela embaixo de outra
```sql
SELECT id
FROM t1
UNION ALL
SELECT id
FROM t2;
```
![Union vs Inner Join](./assets/images/MySQL-UNION-vs-JOIN.png)

---

### Group By
E a ultima coisa importante, `GROUP BY`, essa expressão agrupa os resultados, o forte do group by é utilizar com  `AGGREGATE FUNCTIONS`

---

## Tipos de Dados do MySql
![Tipos de dados](./assets/images/cheatsheet-data_types.png)

---

Links:

1. [Aprenda MySql](http://www.mysqltutorial.org)
2. [Experimente rapido](http://www.mysqltutorial.org/tryit/)
3. [O que é Banco de Dados?](https://dicasdeprogramacao.com.br/o-que-e-um-banco-de-dados/)
4. [MySql CLI cheat sheet](https://gist.github.com/hofmannsven/9164408)
5. [Outra cheat sheet](https://devhints.io/mysql)
6. [A melhor cheat sheet(copiei tudo de lá)](http://www.mysqltutorial.org/mysql-cheat-sheet.aspx)

---

# PHP [clique aqui](./PHP.md) para continuar
