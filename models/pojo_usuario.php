<?php

class PojoUsuario
{

    private $cod_usuario;
    private $nome;
    private $email;
    private $senha;
    private $ativo;

    public function __construct(array $array = [])
    {
        $this->cod_usuario = isset($array['cod_usuario']) ? $array['cod_usuario'] : null;
        $this->nome = isset($array['nome']) ? $array['nome'] : null;
        $this->email = isset($array['email']) ? $array['email'] : null;
        $this->senha = isset($array['senha']) ? $array['senha'] : null;
        $this->ativo = isset($array['ativo']) ? $array['ativo'] : null;
    }

    public function getCod_usuario()
    {
        return $this->cod_usuario;
    }

    public function setCod_usuario($cod_usuario)
    {
        $this->cod_usuario = $cod_usuario;
        return $this;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function setSenha($senha)
    {
        $this->senha = $senha;
        return $this;
    }

    public function getAtivo()
    {
        return $this->ativo;
    }

    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

    public function toArray()
    {
        return [
            'cod_usuario' => $this->cod_usuario,
            'nome' => $this->nome,
            'email' => $this->email,
            'senha' => $this->senha,
            'ativo' => $this->ativo,
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }
}

// $user = new PojoUsuario();
// // or
// $user = new PojoUsuario([
//     'cod_usuario' => 2,
//     'email' => 'foi@gmail.com',
// ]);

// echo $email = $user->getEmail();

// echo $user->toJson();
