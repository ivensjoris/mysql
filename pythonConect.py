import mysql.connector
from mysql.connector import Error

from mysql.connector import MySQLConnection, Error
 
 
def query_with_fetchall():
    try:
        conn = mysql.connector.connect(host='localhost',
                                       database='classicmodels',
                                       user='newuser',
                                       password='user_password')
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM customers")
        rows = cursor.fetchall()
 
        print('Total Row(s):', cursor.rowcount)
        for row in rows:
            print(row)
 
    except Error as e:
        print(e)
 
    finally:
        cursor.close()
        conn.close()
 
 
if __name__ == '__main__':
    query_with_fetchall()
