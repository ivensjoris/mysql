<?php

require_once './admin/dbconfig.php';

try {
    $pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);

    $sql = 'SELECT lastname,
                    firstname,
                    jobtitle
               FROM employees
              WHERE lastname LIKE ?';

    $q = $pdo->prepare($sql);
    $q->execute(['%son']);
    $q->setFetchMode(PDO::FETCH_ASSOC);

    // while ($r = $q->fetch()) {
    //     echo $r["lastname"] . '<br/>';
    // }
    echo '<br/>';

    $employees = $q->fetchAll();

    if (count($employees)) {
        foreach ($employees as $row) {
            echo $row["firstname"] . " " . $row["lastname"] . '<br/>';
        }
    } else {
        echo "Nenhum resultado retornado.";
    }
} catch (PDOException $pe) {
    die("Could not connect to the database $dbname :" . $pe->getMessage());
}

// $sql = 'SELECT lastname,
// firstname,
// jobtitle
// FROM employees
// WHERE lastname LIKE :lname OR
// firstname LIKE :fname;';

// // prepare statement for execution
// $q = $pdo->prepare($sql);

// // pass values to the query and execute it
// $q->execute([':fname' => 'Le%',
// ':lname' => '%son']);
